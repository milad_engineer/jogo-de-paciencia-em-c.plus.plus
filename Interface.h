#include <iostream>
#include <cstdlib>
#include "concol.h"
#include "Pilha Encadeada.h"

using namespace std;

//############ FUN��O CONTA PILHA ##################
int ContaCartas (Pilha &p)
{
    Pilha pilha_aux;
    inicializa_pilha(pilha_aux);
    Carta *c_aux;
    int cont=0;
    if (!pilha_vazia(p))
    {
        while (!pilha_vazia(p))
        {
            c_aux=desempilha(p);
            empilha(pilha_aux,*c_aux);
            cont++;
        }
        int i_aux = cont;/// MUDAR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        while (i_aux!=0)
        {
            c_aux=desempilha(pilha_aux);
            empilha(p,*c_aux);
            i_aux--;

        }
        return (cont);
    } else {
        return 0;
    }
}

//############ FUN��O CONTA CARTAS ABERTAS ##################
int ContaCartasAbertas (Pilha &p)
{
    Pilha pilha_aux;
    inicializa_pilha(pilha_aux);
    Carta *c_aux;
    int cont=0;
    while (!pilha_vazia(p) && p.topo->aberta)
    {
        c_aux=desempilha(p);
        empilha(pilha_aux,*c_aux);
        cont++;
    }
    int i_aux = cont;/// MUDAR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    while (i_aux!=0)
    {
        c_aux=desempilha(pilha_aux);
        empilha(p,*c_aux);
        i_aux--;
    }
    return (cont);
}

void Interface(Pilha *vet)
{
    Carta *c_aux;
    int k=0, posx=0, i_aux=0;
    Pilha pilha_aux;

    cout<<endl<<endl;

    ///Para a pilha de estoque
    if (!pilha_vazia(vet[11]))
    {
        k=ContaCartas(vet[11]);
        gotoxy(1,1);
        cout<<"|ESTOQUE|"<<endl;
        gotoxy(1,2);
        cout<<"|-"<<k<<"CA--|";
    } else {
        gotoxy(1,1);
        cout<<"|ESTOQUE|"<<endl;
        gotoxy(1,2);
        cout<<"|-VAZIO-|";
    }

    ///Para a pilha de descarte
    if (!pilha_vazia(vet[12]))
    {
        if (vet[12].topo->naipe)
        {
            gotoxy(10,1);
            cout<<"|DESCARTE|"<<endl;
            gotoxy(10,2);
            cout<<"|---"<<vet[12].topo->numero<<vet[12].topo->naipe<<"---|";
        } else {
            gotoxy(10,1);
            cout<<"|DESCARTE|"<<endl;
            gotoxy(10,2);
            cout<<"|---XX---|";
        }
    } else {
        gotoxy(10,1);
        cout<<"|DESCARTE|"<<endl;
        gotoxy(10,2);
        cout<<"|-VAZIO--|";
    }

    ///Para imprimir as pilhas de montagem (7-10)
    posx=30;
    for (int i=7; i<=10; i++)
    {
        k=1;
        inicializa_pilha(pilha_aux);
        if (!pilha_vazia(vet[i]))
        {
            inicializa_pilha(pilha_aux);
            while (!pilha_vazia(vet[i]))
            {
                c_aux = desempilha(vet[i]);
                empilha(pilha_aux,*c_aux);
            }
            gotoxy(posx,k);
            cout<<"|MONT "<<i-6<<"|"<<endl;
            while (!pilha_vazia(pilha_aux))
            {
                c_aux = desempilha(pilha_aux);
                empilha(vet[i],*c_aux);
                gotoxy(posx,k+1);
                cout<<"|--"<<c_aux->numero<<c_aux->naipe<<"--|";
                k=k+1;
            }

        } else {
            gotoxy(posx,k);
            cout<<"|MONT "<<i-6<<"|"<<endl;
            gotoxy(posx,k+1);
            cout<<"|VAZIO |";
        }
        posx=posx+8;
    }

    ///Para imprimir as pilhas de jogo (0-6)
    posx=10;
    for (int i=0; i<=6; i++)
    {
        inicializa_pilha(pilha_aux);
        int k=6;
        if (!pilha_vazia(vet[i]))
        {

            while (!pilha_vazia(vet[i]))
            {
                c_aux = desempilha(vet[i]);
                empilha(pilha_aux,*c_aux);
            }
            gotoxy(posx,k);
            cout<<"|PILHA "<<i+1<<"|"<<endl;
            while (!pilha_vazia(pilha_aux))
            {
                c_aux = desempilha(pilha_aux);
                empilha(vet[i],*c_aux);
                if (c_aux->aberta) {
                    gotoxy(posx,k+1);
                    cout<<"|--"<<c_aux->numero<<c_aux->naipe<<"--|";
                } else {
                    gotoxy(posx,k+1);
                    cout<<"|--##---|";
                }
                k=k+1;
            }

        } else {
            gotoxy(posx,k);
            cout<<"|PILHA "<<i+1<<"|"<<endl;
            gotoxy(posx,k+1);
            cout<<"|-VAZIO-|";
        }
        posx=posx+9;
    }

    cout<<endl<<endl;
    system("pause");
}
