/* Universidade Católica Dom Bosco
Curso de Engenharia da Computação
Professor Marcos Alvez
Aluno Milad Roghanian, RA 160985
Trabalho de Estrutura de dados I, Jogo de Paciência
Data de finalização do trabalho -------------------*/

#include <iostream>
#include <cstdlib>
#include <time.h>

using namespace std;

//############ ESTRUTURAS ##################
struct Carta
{
    bool aberta;
    char naipe;
    int numero;
};

struct Pilha
{
    int base, topo;
};

//############ FUNÇÃO INICIALIZA PILHA ##################
void inicializa_pilha(Pilha p)
{
    p.base = 0;
    p.topo = -1;
}

bool pilha_vazia(Pilha p)
{
    return (p.topo<p.base);
}

//############ FUNÇÃO INSERE PILHA ##################
void empilha (Pilha p[], Carta vetcartas[],Carta x, int pos)
{
    int i_aux=0;
    if (!pilha_vazia(p[pos]))
    {
        i_aux =p[pos].topo;
        for (int i=51; i>i_aux; i--)
        {
            vetcartas[i+1]=vetcartas[i];
        }
        for (int i=pos+1; i<=12; i++)
        {
            p[i].base=p[i].base+1;
            p[i].topo=p[i].topo+1;
        }
        p[pos].topo = p[pos].topo+1;
        vetcartas[i_aux+1]=x;

    } else {
        if (pos!=0){
            i_aux =p[pos-1].topo;
        } else {
            i_aux = -1;
        }
        for (int i=51; i>i_aux; i--)
        {
            vetcartas[i+1]=vetcartas[i];
        }
        for (int i=pos+1; i<=12; i++)
        {
            p[i].base=p[i].base+1;
            p[i].topo=p[i].topo+1;
        }
        p[pos].topo = i_aux+1;
        p[pos].base = i_aux+1;
        vetcartas[i_aux+1]=x;
    }
}

//############ FUNÇÃO DESEMPILHA ##################
Carta desempilha(Pilha p[], Carta vetcartas[], int pos)
{
    if (!pilha_vazia(p[pos]))
    {
        //aux = vetcartas[53]

        vetcartas[59] = vetcartas[p[pos].topo];
        int i_aux =p[pos].topo;

        for (int i=i_aux; i<=51; i++)
        {
            vetcartas[i+1]=vetcartas[i];
        }
        if (p[pos].base==p[pos].topo) //Se a pilha for desempilhada e este for o último elemento, será dado pilha vazia
        {
            p[pos].base = 0;
            p[pos].topo = -1;
        } else {
            for (int i=pos; i<=12; i++)
            {
                if (p[i].topo==p[i].base) {
                    p[i].base = 0;
                    p[i].topo = -1;
                } else {
                    p[i].base=p[i].base-1;
                    p[i].topo=p[i].topo-1;
                }
            }
        }
        return vetcartas[59];
    }
    else
    {
        cout<<"\n     PILHA VAZIA 1!   "<<endl;
        system("pause");
    }
}

//############ MODIFICA VALOR ##################
/*void modifica_valor(Pilha &p, int pos, int x)
{
    if (!pilha_vazia(p))
    {
        Pilha aux;
        Carta *c_aux;
        Carta *c_aux2;
        int k=1;
        while (k!=pos && !pilha_vazia(p))
        {
            c_aux=desempilha(p);
            empilha(aux,*c_aux);
            k++;
        }
        c_aux=desempilha(p);
        c_aux->numero=x;
        empilha(aux,*c_aux);
        while (k!=0)
        {
            c_aux2=desempilha(aux);
            empilha(p,*c_aux2);
            k--;
        }
    } else {
        cout<<"\n     PILHA VAZIA!   "<<endl;
        system("pause");
    }
}*/

/*############ FUNÇÃO MOVE ELEMENTOS DA PILHA1 PARA PILHA2 ATÉ CERTA POSIÇÃO ##################
void MovePilhaPosicao(Pilha &p, Pilha &p2, int pos)
{
    //P a pilha que será desempilhada
    //p2 a pilha que será empilhada com elementos da p2
    //pos até que posição a pilha p será desempilhada e empilhada na p2
    if (!pilha_vazia(p))
    {
        Carta *c_aux;
        Carta *c_aux2;
        int k=0;
        while (k!=pos && !pilha_vazia(p))
        {
            c_aux=desempilha(p);
            empilha(p2,*c_aux);
            k++;
        }
    }
}*/


