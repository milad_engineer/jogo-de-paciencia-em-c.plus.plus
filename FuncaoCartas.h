#include <iostream>
#include <cstdlib>
#include "Interface.h"

using namespace std;

///########## FUN��O CRIA CARTAS ##########
///Essa fun��o coloca as 52 cartas com seus naipes na pilha
///que cont�m todas as cartas do jogo mas de forma n�o embaralhada
void CriaCartas (Carta vetcartas[])
{
    int k=51;
    for (int i = 0; i < 4; i++) //os 4 naipes
    {
        for (int j = 0; j < 13; j++) // as 13 cartas de cada naipe
        {
            if (i==0){
                vetcartas[k].naipe=4; //espadas PRETO
                vetcartas[k].numero=j+1;
            } else if (i==1){
                vetcartas[k].naipe=3; //copas VERMELHO
                vetcartas[k].numero=j+1;
            } else if (i==2){
                vetcartas[k].naipe=5; //paus PRETO
                vetcartas[k].numero=j+1;
            } else {
                vetcartas[k].naipe=6; //ouros VERMELHO
                vetcartas[k].numero=j+1;
            }
            k--;
        }
    }
}

///########## FUN��O EMBARALHA CARTAS ##########
///Essa fun��o embaralha as 52 cartas
void EmbaralhaCartas (Carta vetcartas[])
{
    //aux = vetcartas[59]
    srand(NULL);
    for (int i=0; i<=250; i++) //embaralha
    {
        int r = rand()%51; //r recebe rand para escolher uma posi��o aleat�ria do vetor de cartas
        if (r==0){
            r=1;
        }
        vetcartas[59] = vetcartas[r];
        vetcartas[r] = vetcartas[r+1];
        vetcartas[r+1] = vetcartas[59];
    }
    for (int i=0; i<=250; i++)
    {
        int r = rand()%40; //r recebe rand para escolher uma posi��o aleat�ria do vetor de cartas
        if (r==0){
            r=1;
        }
        vetcartas[59] = vetcartas[r];
        vetcartas[r] = vetcartas[r+12];
        vetcartas[r+12] = vetcartas[59];
    }
}

///########## FUN��O DISTRIBUI CARTAS ##########
///Essa fun��o distribui as 52 cartas jogando-as nas 7 pilhas principais do jogo
void DistribuiCartas (Pilha p[])
{
    p[0].base=0;
    p[0].topo=0;
    p[1].base=1;
    p[1].topo=2;
    p[2].base=3;
    p[2].topo=5;
    p[3].base=6;
    p[3].topo=9;
    p[4].base=10;
    p[4].topo=14;
    p[5].base=15;
    p[5].topo=20;
    p[6].base=21;
    p[6].topo=27;
    p[7].base=0;
    p[7].topo=-1;
    p[8].base=0;
    p[8].topo=-1;
    p[9].base=0;
    p[9].topo=-1;
    p[10].base=0;
    p[10].topo=-1;
    p[11].base=28;
    p[11].topo=50;
    p[12].base=51;
    p[12].topo=51;
}

///########## FUN��O INICIA PARTIDA ##########
///Essa fun��o executa as primeiras modifica��es nas pilhas para o in�cio da partida
void IniciaPartida (Pilha p[], Carta vetcartas[])
{
   Carta c_aux;
   for (int i=0; i<=6; i++) //abre as primeiras cartas de cada pilha
   {
        vetcartas[p[i].topo].naipe = true;
   }
   vetcartas[p[12].topo].naipe = true;
}

///########## FUN��O TRANSFERE PILHA EMBARALHADA ##########
///Essa fun��o age quando a pilha de estoque (11) estiver vazia,
///desempilhando a pilha de descarte e recolocando na pilha de estoque de forma embaralhada
void TranferePilhaEmb (Pilha p[], Carta vetcartas[])//p1 � a Pilha a ser preenchida com os elementos da p2 de forma embaralhada
{
    //Pilha p11 de estoque
    //Pilha p12 de descarte
    if(pilha_vazia(p[11]) && !pilha_vazia(p[12]))
    {
        //aux = vetcartas[58]

        while (!pilha_vazia(p[12]))      //Enquanto pilha de descarte n�o estiver vazia o while � executado at� todas
        {                                //as cartas da pilha de descarte forem movidas para a pilha de estoque
            vetcartas[58] = desempilha(p,vetcartas,12);
            empilha(p,vetcartas,vetcartas[58],12);
        }
    } else if (pilha_vazia(p[12]) && !pilha_vazia(p[11])) {
        cout<<"Pilha de descarte vazia, ainda ha cartas na pilha de estoque";
        ///TERMINAR!!!!!!!!!!!!!!!
        system("pause");
    } else if (pilha_vazia(p[11]) && pilha_vazia(p[12])){
        cout<<"Nao ha cartas nas pilhas de estoque e descarte";
        system("pause");
    } else {
        cout<<"Ha cartas nas duas pilhas";
        system("pause");
    }
}

///########## FUN��O REP�E PILHA DE DESCARTE ##########
///Essa fun��o desempilha uma carta da pilha de estoque (se esta n�o estiver vazia)
///e empilha na pilha de descarte se esta estiver vazia
bool RepoePilha (Pilha p[], Carta vetcartas[])
{
    //Pilha p11 a se desempilhar 1 carta
    //Pilha p12 a ser reposta com a carta da p11
    //aux = vetcartas[58]

    if (!pilha_vazia(p[11]) && pilha_vazia(p[12])) { //se a pilha de estoque n�o estiver vazia e a de descarte estiver vazia
        vetcartas[58] = desempilha(p,vetcartas,11); //� desempilhado uma carta dessa pilha
        vetcartas[58].aberta=true;
        empilha(p,vetcartas,vetcartas[58],12); //e empilhado na pilha de descarte
        return true; //retorna true se a a��o for executada
    } else if (!pilha_vazia(p[11])) {
        return false; //sen�o retorna false
    }
}

///Essa fun��o desempilha uma carta da pilha de estoque (se esta n�o estiver vazia)
///e empilha na pilha de descarte se esta estiver vazia
bool RepoePilhaQualquer (Pilha p[], Carta vetcartas[], int pos1, int pos2)
{
    //Pilha pos1 a se desempilhar 1 carta
    //Pilha pos2 a ser reposta com a carta da p
    //aux = vetcartas[58]

    if (!pilha_vazia(p) && !pilha_vazia(p2)) { //se a pilha de estoque n�o estiver vazia e a de descarte estiver vazia
        vetcartas[58] = desempilha(p,vetcartas,p[pos1]); //� desempiplhado uma carta dessa pilha
        vetcartas[58].aberta=true;
        empilha(p,vetcartas,vetcartas[58],p[pos2]); //e empilhado na pilha de descarte
        return true; //retorna true se a a��o for executada
    } else {
        return false; //sen�o retorna false
    }
}

///########## FUN��O VIRA A CARTA ##########
///Essa fun��o atribui true(vira a carta para cima) a carta do topo da pilha se esta n�o estiver vazia
void ViraCarta (Pilha p[], Carta vetcartas[], int pos)
{
    if (!pilha_vazia(p[pos]))
    {
        vetcartas[p[pos].topo].aberta=true;
    }
}

///########## FUN��O VERIFICA NAIPE ##########
///Essa fun��o verifica se o nipe das duas cartas s�o de cores diferentes
bool VerificaNaipe (Carta vetcartas[], int c1, int c2)
{
    if ((vetcartas[c1].naipe<=4 && vetcartas[c2].naipe>4))
    {
        return true;
    } else if ((vetcartas[c1].naipe>4 && vetcartas[c2].naipe<=4)) {
        return true;
    } else {
        return false;
    }
}

///########## FUN��O VERIFICA INSER��O ##########
///Essa fun��o tem a finalidade de verificar entre as pilhas que tem cartas abertas a possibilidade de
///inser��o nas pilhas de 7 a 10 que s�o as de montagem em ordem
bool VerificaInsercao (Pilha p[], Carta vetcartas[])
{
    gotoxy(1,20);
    //aux = vetcartas[57]
    ///------------------ PRIMEIRA PARTE DA FUN��O, CASO SE ENCONTRE UM "A" PARA SE INSERIR------------------------------
    if (vetcartas[p[12].topo].numero==1 && vetcartas[p[12].topo].aberta && !pilha_vazia(p[12])) //Se a carta do topo da pilha de descarte estiver aberta for um "A" e n�o estiver vazia
    {
        ///VERIFICA-SE NA PILHA DE DESCARTE
        for (int i=7; i<=10; i++)
        {
            if (pilha_vazia(p[i])) {                 //Dentro desses if's verifica-se a possibilidade de inserir uma carta
                vetcartas[57] = desempilha(p,vetcartas,12);             //em uma das 4 pilhas de montagem se essas estiverem vazias e a carta a ser inserida for um "A"
                empilha(p,vetcartas,vetcartas[57],i);
                cout<<"A carta "<<vetcartas[57].numero<<vetcartas[57].naipe<<" foi movida do DESCARTE para o MONTE "<<i-6<<endl;
                system("pause");
                system("cls");
                Interface(p,vetcartas);
                if (RepoePilha(p,vetcartas)) {//se a carta for reposta com sucesso
                    //N�o se executa nada
                } else {
                    TranferePilhaEmb(p,vetcartas));//sen�o pede-se para repor a pilha de estoque
                }
                return true; //retorna-se verdadeiro se a a��o for executada
            }
        }
    } else { //Se a carta do topo da pilha de descarte n�o for um "A"

        ///SE N�O, VERIFICA-SE NAS PILHAS DE JOGO (0-6)
        for (int i=0; i<=6; i++) //Percorrendo entre as 6 pilhas de jogo procurando um "A" para inserir em uma pilha de montagem
        {
            if (!pilha_vazia(p[i]) && vetcartas[p[i].topo].aberta && vetcartas[p[i].topo].numero==1) //enquanto a pilha i n�o estiver vazia e aberta e o topo sendo a carta "A" entra-se no while
            {
                for (int j=7; j<=10; j++) //percorre as pilhas de montagem (7-10) para verificar se � poss�vel inserir
                {
                    if (pilha_vazia(p[j])) { //tenta inserir em uma das pilhas de montagem se estas estiverem vazias
                        vetcartas[57] = desempilha(p,vetcartas,i);
                        empilha(p,vetcartas,vetcartas[57],j);
                        cout<<"A carta "<<vetcartas[57].numero<<vetcartas[57].naipe<<" foi movida da PILHA "<<i+1<<" para o MONTE "<<j-6<<endl;
                        system("pause");
                        system("cls");
                        Interface(p,vetcartas);
                        ViraCarta(p,vetcartas,i); //manda para essa fun��o para abrir a carta debaixo j� que a de cima foi desempilhada
                        return true; //retorna-se verdadeiro se a a��o for executada
                    }
                } //FIM FOR (7-10)
            } //FIM WHILE
        } //FIM FOR (0-6)
    } //FIM DO ELSE DO IF MAIOR

    ///------------------ SEGUNDA PARTE DA FUN��O, QUALQUER QUE SEJA A CARTA PARA SE INSERIR------------------------------

    ///VERIFICA-SE NA PILHA DE DESCARTE
    if (!pilha_vazia(p[12])) //Se a pilha de descarte n�o estiver vazia
    {
        for (int i=7; i<=10; i++) //Percorrendo as 4 pilhas de montagem e verificando se � poss�vel inserir a carta do topo da pilha de descarte nelas
        {
            //Se a pilha de montagem n�o estiver vazia, o naipe for igual e a carta for um n�mero maior
            if (!pilha_vazia(p[i]) vetcartas[p[12].topo].naipe==vetcartas[p[i].topo].naipe && vetcartas[p[12].topo].numero==vetcartas[p[i].topo].numero+1) {
                vetcartas[57] = desempilha(p,vetcartas,12);
                empilha(p,vetcartas,vetcartas[57],i);
                cout<<"A carta "<<vetcartas[57].numero<<vetcartas[57].naipe<<" foi movida do DESCARTE para o MONTE "<<i-6<<endl;
                system("pause");
                system("cls");
                Interface(p,vetcartas);
                if (RepoePilha(p,vetcartas)) {//se a carta for reposta com sucesso
                    //N�o se executa nada
                } else {
                    TranferePilhaEmb(p,vetcartas);//sen�o, pede-se para repor a pilha de estoque
                }
                return true; //retorna-se verdadeiro se a a��o for executada
            }
        }
    }
    ///VERIFICA-SE NAS PILHAS DE JOGO (0-6)
    for (int i=0; i<=6; i++) //Percorrendo entre as 6 pilhas de jogo
    {
        if (!pilha_vazia(p[i]) && vetcartas[p[i].topo].aberta) //enquanto o topo da pilha i estiver aberta e as pilhas de jogo n�o estiverem vazias
        {
            for (int j=7; j<=10; j++) //percorre as pilhas de montagem (7-10) para verificar se � poss�vel inserir
            {
                if (!pilha_vazia(p[j]) && vetcartas[p[i].topo].naipe==vetcartas[p[j].topo].naipe && vetcartas[p[i].topo].numero==vetcartas[p[j].topo].numero+1) { //tenta inserir em uma das pilhas de montagem se estas estiverem vazias
                    vetcartas[57] = desempilha(p,vetcartas,i);
                    empilha(p,vetcartas,vetcartas[57],j);
                    cout<<"A carta "<<vetcartas[57].numero<<vetcartas[57].naipe<<" foi movida da PILHA "<<i+1<<" para o MONTE "<<j-6<<endl;
                    system("pause");
                    system("cls");
                    Interface(p,vetcartas);
                    ViraCarta(p,vetcartas,i); //manda para essa fun��o para abrir a carta debaixo j� que a de cima foi desempilhada
                    return true; //retorna-se verdadeiro se a a��o for executada
                }
            } //FIM FOR (7-10)
        } //FIM WHILE
    } //FIM FOR (0-6)
    return false; //Se nenhuma a��o for executada, retorna-se falso
} // FIM DA FUN��O


///########## FUN��O MOVIMENTA CARTAS ENTRE PILHAS ##########
///Essa fun��o tem a finalidade de movimentar as cartas entre as pilhas de jogo e de descarte
///dentro das regras e dentro do poss�vel
bool MovimentaCartas (Pilha *vet)
{
    int i_aux=0, conta_jogadas=0;
    Pilha pilha_aux;
    Carta *c_aux;
    bool ajudante;
    for (int i=0; i<=6; i++)//Aqui o For roda at� serem verificadas todas as pilhas de jogo (0-6)
    {
        if (!pilha_vazia(vet[i])) //Se a pilha a ser verficada n�o estiver vazia
        {
            inicializa_pilha(pilha_aux);
            i_aux = ContaCartasAbertas(vet[i]); //manda para a fun��o contar quantas cartas abertas existem nesta pilha
            MovePilhaPosicao(vet[i],pilha_aux,i_aux);
            for(int j=0; j<i_aux; j++) //Aqui o For roda at� desempilhar e verificar todas as cartas abertas da pilha
            {
                c_aux = desempilha(pilha_aux); //desempilha-se a carta do topo para ser verificada
                ajudante = true;

                for (int k=0; k<=6; k++) //Este For roda at� a carta selecionada ser verifica entre cada uma das outras pilhas de jogo
                {
                    //Verifica-se a poss�vel inser��o da carta no topo de uma das outras pilhas de jogo
                    if (ajudante && i!=k && !pilha_vazia(vet[k]) && vet[k].topo->aberta && c_aux->numero+1==vet[k].topo->numero && VerificaNaipe(*c_aux,*vet[k].topo))
                    {
                        empilha(vet[k],*c_aux);
                        MovePilhaPosicao(pilha_aux,vet[k],i_aux-j);
                        ViraCarta(vet[i]);
                        cout<<i_aux-j<<" Cartas da PILHA "<<i+1<<" foram movidas para a PILHA "<<k+1<<endl;
                        system("pause");
                        system("cls");
                        Interface(vet);
                        ajudante = false;
                        conta_jogadas++;
                        inicializa_pilha(pilha_aux);
                    }
                }
                if (ajudante)
                {
                    empilha(vet[i],*c_aux);  //empilha-se em uma pilha auxiliar para n�o haver perda da carta
                }
            }
        }
    }
    if (conta_jogadas!=0)
    {
        return true;
    } else {
        cout<<"!!!NENHUM MOVIMENTO REALIZADO!!!"<<endl;
        system("pause");
        return false;
    }
}

///########## FUN��O MOVIMENTA REI ##########
///Essa fun��o tem a finalidade de movimentar o rei para uma das pilhas quando esta estiver vazia
bool MovimentaRei (Pilha *vet)
{
    int i_aux=0, conta_jogadas=0;
    Pilha pilha_aux;
    Carta *c_aux;
    bool ajudante;
    for (int i=0; i<=6; i++)//Aqui o For roda at� serem verificadas todas as pilhas de jogo (0-6)
    {
        if (pilha_vazia(vet[i])) //Se a pilha a ser verficada n�o estiver vazia
        {
            for (int j=0; j<=6; j++)
            {
                if (j!=i && !pilha_vazia(vet[j])) {
                    inicializa_pilha(pilha_aux);
                    i_aux = ContaCartasAbertas(vet[j]); //manda para a fun��o contar quantas cartas abertas existem nesta pilha
                    MovePilhaPosicao(vet[j],pilha_aux,i_aux);
                    for(int k=0; k<i_aux; k++) //Aqui o For roda at� desempilhar e verificar todas as cartas abertas da pilha
                    {
                        c_aux = desempilha(pilha_aux); //desempilha-se a carta do topo para ser verificada
                        ajudante = true;
                        if (pilha_vazia(vet[i]) && c_aux->numero==13)
                        {
                            empilha(vet[i],*c_aux);
                            MovePilhaPosicao(pilha_aux,vet[i],i_aux-k);
                            ViraCarta(vet[j]);
                            cout<<i_aux-k<<" Cartas da PILHA "<<j+1<<" foram movidas para a PILHA "<<i+1<<endl;
                            system("pause");
                            system("cls");
                            Interface(vet);
                            ajudante = false;
                            conta_jogadas++;
                            inicializa_pilha(pilha_aux);
                        }
                    }
                    if (ajudante) {
                        empilha(vet[j],*c_aux);  //empilha-se em uma pilha auxiliar para n�o haver perda da carta
                    }
                }
            }
        }
    }
    if (conta_jogadas!=0)
    {
        return true;
    } else {
        cout<<"!!!NENHUM REI MODIFICADO!!!"<<endl;
        system("pause");
        return false;
    }
}

///########## FUN��O PEGA DESCARTE  ##########
///Essa fun��o pega uma carta do descarte e coloca em uma das 7 pilhas de jogo
bool UsaDescarte (Pilha *vet)
{
    int conta_jogadas=0;
    Carta *c_aux;
    if (!pilha_vazia(vet[12]))//Aqui o For roda at� serem verificadas todas as pilhas de jogo (0-6)
    {
        for (int i=0; i<=6; i++) //Se a pilha a ser verficada n�o estiver vazia
        {
            //Verifica-se a poss�vel inser��o da carta no topo de uma das outras pilhas de jogo
            if (!pilha_vazia(vet[i]) && vet[i].topo->aberta && vet[12].topo->numero+1==vet[i].topo->numero && VerificaNaipe(*vet[12].topo,*vet[i].topo))
            {
                c_aux=desempilha(vet[12]);
                empilha(vet[i],*c_aux);
                if (RepoePilha(vet[11],vet[12])) {//se a carta for reposta com sucesso
                    //N�o se executa nada
                } else {
                    TranferePilhaEmb(vet[11],vet[12]);//sen�o pede-se para repor a pilha de estoque
                }
                cout<<"A Carta "<<c_aux->numero<<c_aux->naipe<<" do DESCARTE foi movido para a PILHA "<<i+1;
                system("pause");
                system("cls");
                Interface(vet);
                conta_jogadas++;
            }
        }
    }

    if (conta_jogadas!=0)
    {
        return true;
    } else {
        cout<<"!!!NADA DO DESCARTE!!!"<<endl;
        system("pause");
        return false;
    }
}

///########## FUN��O PEGA DESCARTE  ##########
///Essa fun��o pega uma carta do descarte e coloca em uma das 7 pilhas de jogo
bool UsaDescarteNovamente (Pilha *vet)
{
    int conta_jogadas=0;
    Carta *c_aux;
    if (!pilha_vazia(vet[12]))//Aqui o For roda at� serem verificadas todas as pilhas de jogo (0-6)
    {
        if (RepoePilha2(vet[11],vet[12])) {//se a carta for reposta com sucesso
            //N�o se executa nada
        } else {
            TranferePilhaEmb(vet[11],vet[12]);//sen�o pede-se para repor a pilha de estoque
        }
        for (int i=0; i<=6; i++) //Se a pilha a ser verficada n�o estiver vazia
        {
            //Verifica-se a poss�vel inser��o da carta no topo de uma das outras pilhas de jogo
            if (!pilha_vazia(vet[i]) && vet[i].topo->aberta && vet[12].topo->numero+1==vet[i].topo->numero && VerificaNaipe(*vet[12].topo,*vet[i].topo))
            {
                c_aux=desempilha(vet[12]);
                empilha(vet[i],*c_aux);
                if (RepoePilha(vet[11],vet[12])) {//se a carta for reposta com sucesso
                    //N�o se executa nada
                } else {
                    TranferePilhaEmb(vet[11],vet[12]);//sen�o pede-se para repor a pilha de estoque
                }
                cout<<"A Carta "<<c_aux->numero<<c_aux->naipe<<" do DESCARTE foi movido para a PILHA "<<i+1;
                system("pause");
                system("cls");
                Interface(vet);
                conta_jogadas++;
            }
        }
    }

    if (conta_jogadas!=0)
    {
        return true;
    } else {
        cout<<"!!!NADA DO DESCARTE!!!"<<endl;
        system("pause");
        return false;
    }
}

